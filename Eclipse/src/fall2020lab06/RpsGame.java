// David Tran 1938381

package fall2020lab06;

import java.util.Random;

public class RpsGame {
	private int winsCount = 0;
	private int tiesCount = 0;
	private int lossesCount = 0;
	private Random computerNumber;
	
	public int getWins() {
		return this.winsCount;
	}
	
	public int getTies() {
			return this.tiesCount;
	}
	
	public int getLosses() {
		return this.lossesCount;
	}
	
	public void startGame() {
		Random rand = new Random();
		this.computerNumber = rand;
	}
	
	public String playRound(String playerRPS) {
		int rpsNum = computerNumber.nextInt(3);
		String computerRPS = "";
		String roundEnd = "";
		
		if(rpsNum == 0) {
			computerRPS = "rock";
		}
		
		else if(rpsNum == 1) {
			computerRPS = "paper";
		}
		
		else { // rpsNum == 2
			computerRPS = "scissors";
		}
		
		
		
		if(playerRPS.equals("rock")) {
			if(computerRPS.equals("rock")) {
				this.tiesCount = this.tiesCount + 1;
				roundEnd = "Computer plays rock and it's a tie!";
			}
			
			else if(computerRPS.equals("paper")) {
				this.lossesCount = this.lossesCount + 1;	
				roundEnd = "Computer plays paper and computer won!";
			}
			
			else{ // computerRPS.equals("scissors")
				this.winsCount = this.winsCount + 1;
				roundEnd = "Computer plays scissors and you won!";
			}
		}
		
		else if(playerRPS.equals("paper")) {
			if(computerRPS.equals("rock")) {
				this.winsCount = this.winsCount + 1;
				roundEnd = "Computer plays rock and you won!";
			}
			
			else if(computerRPS.equals("paper")) {
				this.tiesCount = this.tiesCount + 1;
				roundEnd = "Computer plays paper and it's a tie!";
			}
			
			else{ // computerRPS.equals("scissors")
				this.lossesCount = this.lossesCount + 1;
				roundEnd = "Computer plays scissors and computer won!";
			}
		}
		
		else { // playerRPS.equals("scissors")
			if(computerRPS.equals("rock")) {
				this.lossesCount = this.lossesCount + 1;
				roundEnd = "Computer plays rock and computer won!";
			}
			
			else if(computerRPS.equals("paper")) {
				this.winsCount = this.winsCount + 1;
				roundEnd = "Computer plays paper and you won!";
			}
			
			else{ // computerRPS.equals("scissors")
				this.tiesCount = this.tiesCount + 1;
				roundEnd = "Computer plays scissors and it's a tie!";
			}
		}
		
		return roundEnd;
	}
}