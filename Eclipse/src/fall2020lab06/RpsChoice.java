// David Tran 1938381

package fall2020lab06;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField message;
	private TextField winsNum;
	private TextField tiesNum;
	private TextField lossesNum;
	private String playerRPS;
	private RpsGame currentRpsGame;
	
	
	public RpsChoice(TextField roundMessage, TextField wins, TextField ties, TextField losses, String playerChoice, RpsGame currentGame) {
		this.winsNum = wins;
		this.tiesNum = ties;
		this.lossesNum = losses;
		this.playerRPS = playerChoice;
		this.message = roundMessage;
		this.currentRpsGame = currentGame;
		currentGame.playRound(playerChoice);
	}
	
	public void handle(ActionEvent e) {	
		message.setText(this.currentRpsGame.playRound(this.playerRPS));
		winsNum.setText("wins: " + this.currentRpsGame.getWins());
		tiesNum.setText("ties: " + this.currentRpsGame.getTies());
		lossesNum.setText("losses: " + this.currentRpsGame.getLosses());
	}
}
