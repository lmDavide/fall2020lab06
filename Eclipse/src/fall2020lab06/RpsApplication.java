// David Tran 1938381

package fall2020lab06;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {
	private RpsGame currentGame;
	
	public void start(Stage stage) {
		RpsGame newGame = new RpsGame();
		newGame.startGame();
    	currentGame = newGame;
		
		Group root = new Group(); 
		
		VBox newVbox = new VBox();
		HBox newHbox1 = new HBox();
		HBox newHbox2 = new HBox();
		HBox newHbox3 = new HBox();
		Button button1 = new Button("rock");
		Button button2 = new Button("paper");
		Button button3 = new Button("scissors");
		TextField wins = new TextField("wins");
		TextField ties = new TextField("ties");
		TextField losses = new TextField("losses");
		TextField outcome = new TextField("Pick a weapon! >:D");
		
		outcome.setMinWidth(500);
		
		RpsChoice rockChoice = new RpsChoice(outcome, wins, ties, losses, "rock", currentGame);
		RpsChoice paperChoice = new RpsChoice(outcome, wins, ties, losses, "paper", currentGame);
		RpsChoice scissorsChoice = new RpsChoice(outcome, wins, ties, losses, "scissors", currentGame);

		button1.setOnAction(rockChoice);
		button2.setOnAction(paperChoice);
		button3.setOnAction(scissorsChoice);
		
		newHbox1.getChildren().addAll(button1, button2, button3);
		newHbox2.getChildren().addAll(wins, ties, losses);
		newHbox3.getChildren().add(outcome);
		newVbox.getChildren().addAll(newHbox1, newHbox2, newHbox3);
		root.getChildren().add(newVbox);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
        
    }
}    