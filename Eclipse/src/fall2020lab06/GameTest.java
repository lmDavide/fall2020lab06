package fall2020lab06;

public class GameTest {
	public static void main(String[]args) {
		RpsGame newGame = new RpsGame();
		newGame.startGame();
		System.out.println(newGame.playRound("rock"));
		System.out.println(newGame.getWins());
		System.out.println(newGame.getTies());
		System.out.println(newGame.getLosses());
	}
}
